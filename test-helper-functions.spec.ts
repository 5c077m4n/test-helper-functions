import { TestBed, TestModuleMetadata } from '@angular/core/testing';


const resetTestingModule = TestBed.resetTestingModule.bind(TestBed);
const disallowAngularToReset = () => TestBed.resetTestingModule = () => TestBed;
const allowAngularToReset = () => {
	resetTestingModule();
	TestBed.resetTestingModule = resetTestingModule;
};

/**
 * @function setUpTestBed creates a TestBed witch does **not** reset after every afterEach call (unsafe)
 * @param moduleDef the test module metadata needed to init the TestBed.
 * @param beforeAllFn a function or array of functions to run in the beforeAll function.
 * @param afterAllFn a function or array of functions to run in the afterAll function.
 * @description call this function in the description function, outside of any onther function.
 * 
 * ```javascript
 * describe('Test description...', () => {
 * 	setUpTestBed(
 * 		{
 *			// module metadata
 *			imports: [
 *				// ...
 *			],
 *			providers: [
 *				// ...
 *			],
 *			declarations: [
 *				// ...
 *			],
 * 		},
 * 		// function(s) to run in beforeAll()
 * 		,
 * 		// function(s) to run in afterAll()
 * 	);
 * 
 * 	it('should...', () => {
 * 		// testing
 * 	});
 * })
 * ```
 */
export const setUpTestBed = (
	moduleDef: TestModuleMetadata, beforeAllFn?: any, afterAllFn?: any
) => {
	beforeAll(async (done: DoneFn) => {
		resetTestingModule();
		disallowAngularToReset();
		TestBed.configureTestingModule(moduleDef);
		if (beforeAllFn) {
			if (beforeAllFn.constructor === Array) beforeAllFn.forEach(fn => fn());
			else beforeAllFn();
		}
		disallowAngularToReset();
		return await TestBed.compileComponents()
			.then(done)
			.catch(done.fail);
	});

	afterAll(() => {
		allowAngularToReset();
		if (afterAllFn) {
			if (afterAllFn.constructor === Array) afterAllFn.forEach(fn => fn());
			else afterAllFn();
		}
	});
};
